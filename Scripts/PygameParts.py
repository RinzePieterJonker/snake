#!/env python3
"""

"""

# IMPORTS
import pygame as pg

# METADATA
__author__ = "Rinze-Pieter Jonker"
__status__ = "Finished"

# GLOBALS
pg.init()
COLOR_INACTIVE = pg.Color(0, 0, 0)# 'lightskyblue3'
COLOR_ACTIVE = pg.Color(0, 0, 200) # 'dodgerblue2'
COLOR_LOCKED = pg.Color(128, 128, 128)
COLOR_ERROR = pg.Color(255, 0, 0)
FONT = pg.font.Font(None, 32)

# CODE
class TextBox:
	"""
	This class can be used to create a box with text inside it using the pygame package
	This class can only show static text and cannot be used  as a button

	:param x: the x coord of the top left corner for text box, this needs to be an Integer
	:param y: the y coord of the top left corner for the textbox, this needs to be an Integer
	:param width: the length of the text box, this needs to be an Integer
	:param height: the height of the text box, this needs to be an Integer
	:param text: the text inside of the box, this needs to be a String and can be left out, default is an empty String
	:param color: the color of the border of the text box, this can be left empty.
	 			  This needs to be an pygame.Color object
	"""
	def __init__(self, x: int, y: int, width: int, height: int, text = "", border_color=COLOR_INACTIVE, font = FONT):
		self.rect = pg.Rect(x, y, width, height)
		self.text = text
		self.color = border_color
		self.font = font
		self.text_surface = font.render(text, True, COLOR_INACTIVE)
		self.active = False
		self.locked = False

	def draw(self, screen: pg.Surface):
		"""
		This function draws the textbox onto the game window

		:param screen: the pg.Surface object on which the game screen is built
		"""
		# Blit the text.
		screen.blit(self.text_surface, (self.rect.x + 5, self.rect.y + 5))
		# Blit the rect.
		pg.draw.rect(screen, self.color, self.rect, 2)

	def set_text(self, screen: pg.Surface, text = None, color = COLOR_ERROR):
		"""
		This function can be used to change the text or the text color in the textbox

		:param text: the new text that needs to be in the text box
		:param screen:
		:param color:
		:return:
		"""
		if text is not None:
			self.text = text
		self.text_surface = self.font.render(text, True, color)

		screen.blit(self.text_surface, (self.rect.x + 5, self.rect.y + 5))
		pg.draw.rect(screen, self.color, self.rect, 2)

	def get_text(self):
		return self.text


class EntryBox(TextBox):
	def __init__(self, x: int, y: int, width: int, height: int, max_char: int, text = "", border_color=COLOR_INACTIVE, font = FONT):
		self.rect = pg.Rect(x, y, width, height)
		self.text = text
		self.color = border_color
		self.font = font
		self.text_surface = font.render(text, True, COLOR_INACTIVE)
		self.active = False
		self.locked = False
		self.max_char = max_char

	def handle_event(self, event):
		if not self.locked:
			if event.type == pg.MOUSEBUTTONDOWN:
				# if the box is clicked and changing the active state
				if self.rect.collidepoint(event.pos):
					self.active = not self.active
				else:
					self.active = False
				self.color = COLOR_ACTIVE if self.active else COLOR_INACTIVE

			if event.type == pg.KEYDOWN:
				# when the user types in the box
				if self.active:
					if event.key == pg.K_RETURN:
						self.text = ""
					elif event.key == pg.K_BACKSPACE:
						self.text = self.text[:-1]
					else:
						self.text += event.unicode

					if len(self.text) > self.max_char:
						self.text = self.text[1:]

					self.text_surface = FONT.render(self.text, True, self.color)

	def draw(self, screen: pg.Surface):
		# Blit the text.
		screen.blit(self.text_surface, (self.rect.x + 5, self.rect.y + 5))
		# Blit the rect.
		pg.draw.rect(screen, self.color, self.rect, 2)

	def set_locked(self, state: bool):
		self.locked = state
		# if state:
		# 	self.color = COLOR_LOCKED
		# else:
		# 	self.color = COLOR_INACTIVE


class Button(TextBox):
	def __init__(self, x: int, y: int, width: int, height: int, function, font = FONT, text = "", border_color = COLOR_INACTIVE):
		self.rect = pg.Rect(x, y, width, height)
		self.surface = pg.Surface((width, height))

		self.text = text
		self.color = border_color
		self.font = font
		self.text_surface = self.font.render(text, True, COLOR_INACTIVE)
		self.active = False
		self.function = function

	def handle_event(self, event):
		if event.type == pg.MOUSEBUTTONDOWN:
			if self.rect.collidepoint(event.pos):
				self.function()

		# if event.type == pg.MOUSEMOTION:
		# 	if self.rect.collidepoint(event.pos):
		# 		self.surface.fill((255, 0, 0), self.rect)
		# 	else:
		# 		self.surface.fill((0, 0, 255), self.rect)