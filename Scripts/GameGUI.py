#!/env python3

"""

"""

# METADATA
__author__ = "Rinze-Pieter Jonker"
__version__ = 0.5
__status__ = "Work In Progress"

# IMPORTS
import random

import pygame as pg
import numpy as np
import PygameParts as pgparts

from exceptions import *

# GLOBALS
pg.init()
screen = 0

running = True
auto = False

auto_solve_button = 0
snake = 0
food = 0

settings = {}


# CODE
class Snake:
	"""
	This class is used as the snake in the snake game and has its own update, event handling and reset function

	:param starting_coords: the starting coordinates for the snake [x, y]
	:param resolution: the resolution of the screen [x, y]
	"""
	def __init__(self, starting_coords: list, resolution):
		self.starting_coords = starting_coords
		self.resolution = resolution
		self.length = 1 # The length of the snake and also the score
		self.direction = "" # The direction of where the snake is going, default is still
		self.color = pg.Color(0, 255, 0) # The color of the snake

		self.coords = np.array(starting_coords)
		self.snake = [pg.Rect(self.coords[0], self.coords[1], 20, 20)]
		self.all_coords = [self.coords.tolist()]

	def draw(self, screen: pg.Surface):
		"""
		This function is used to draw the snake

		:param screen: the pygame surface object on which the snake must be drawn
		"""
		for rect in self.snake:
			pg.draw.rect(screen, self.color, rect, 0)

	def handle_event(self, event):
		"""
		This function is used to handle the events that are given

		:param event: the pygame event which is given
		"""
		if event.type == pg.KEYDOWN:
			# only checking if a key is pressed
			if event.key == pg.K_DOWN or event.key == pg.K_s:
				# checking if the key that is being pressed is bound to an action
				if self.length == 1 or self.direction != "Up":
					# preventing a movement into itself
					self.direction = "Down"

			# repeating the above code for each other movement
			elif event.key == pg.K_UP or event.key == pg.K_w:
				if self.length == 1 or self.direction != "Down":
					self.direction = "Up"
			elif event.key == pg.K_RIGHT or event.key == pg.K_d:
				if self.length == 1 or self.direction != "Left":
					self.direction = "Right"
			elif event.key == pg.K_LEFT or event.key == pg.K_a:
				if self.length == 1 or self.direction != "Right":
					self.direction = "Left"

	def move(self):
		"""
		This function is handles the movement of the snake. This function also checks for collisions with the wall or
		itself
		"""
		# handling the direction based on the movement
		if self.direction == "Down":
			self.coords += 0, 20
		elif self.direction == "Up":
			self.coords -= 0, 20
		elif self.direction == "Left":
			self.coords -= 20, 0
		elif self.direction == "Right":
			self.coords += 20, 0

		# Checking for the vertical border checking
		if self.coords[1] >= self.resolution[1]:
			self.coords[1] = 0
		elif self.coords[1] <= 0:
			self.coords[1] = self.resolution[1]

		# Checking for horizontal border crossing
		if self.coords[0] >= self.resolution[0]:
			self.coords[0] = 0
		elif self.coords[0] <= 0:
			self.coords[0] = self.resolution[0]

		# Creating a new rectangle in front of the snake
		new_rect = [pg.Rect(self.coords[0] + 1, self.coords[1] + 1, 18, 18)]

		# Checking if the new rectangle collides with the snake itself, if it collides it raises an error
		if self.coords.tolist() in self.all_coords and self.direction != "":
			raise CollisionError("Collision detected")

		# Adding the rectangle to the snake and removing the last rectangle if the length of the snake is longer than score
		self.snake = new_rect + self.snake
		if len(self.snake) > self.length:
			self.snake.pop()

		# Doing the same for the list that saves the coords of the rectangles
		self.all_coords = [self.coords.tolist()] + self.all_coords
		if len(self.all_coords) > self.length:
			self.all_coords.pop()

	def check_food_collision(self, food):
		"""
		this function is used to check for collisions with the food blocks, if there is a collision detected than it will
		add one point

		:param food: the food object that is currently in the game
		"""
		if food.coords == self.coords.tolist():
			self.length += 1

			food.create_new(self.all_coords)

	def reset(self, length = 1):
		"""
		This function resets the snake, there can also be a length given for how long it must be
		:param length: Int, this will set the length for the snake
		:return:
		"""
		self.length = length

		while self.length < len(self.all_coords):
			self.all_coords.pop()

		while self.length < len(self.snake):
			self.snake.pop()


class Food:
	"""
	This class handles the functions for the food item in the game

	:param resolution: the resolution of the game [x, y]
	"""
	def __init__(self, resolution):
		self.resolution = resolution

		# Setting random coordinates for the coordinates
		self.coords = [random.randrange(0, self.resolution[0], 20), random.randrange(0, self.resolution[1], 20)]
		self.rect = pg.Rect(self.coords[0], self.coords[1], 20, 20)
		self.eaten = False
		self.color = pg.Color(255, 0, 0)

	def draw(self, screen: pg.Surface):
		"""
		This function draws the food class on the screen

		:param screen: the screen object on which the food is being drawn
		"""
		pg.draw.rect(screen, self.color, self.rect, 0)

	def create_new(self, unavail: list):
		"""
		This function is used to move the food item to a new random position

		:param unavail: a list with all the coordinates where the food can't be moved to
		:return:
		"""
		in_list = True

		# Creating new coordinates and checking if it is available, if it is not available it will create a new set of coordinates
		self.coords = [random.randrange(0, self.resolution[0], 20), random.randrange(0, self.resolution[1], 20)]
		while in_list:
			if not self.coords in unavail:
				in_list = False
			else:
				self.coords = [random.randrange(0, self.resolution[0], 20), random.randrange(0, self.resolution[1], 20)]

		self.rect = pg.Rect(self.coords[0], self.coords[1], 20, 20)


def load_settings():
	settings = dict()

	settings["size"] = np.array([100, 70])
	settings["game_speed"] = 8
	return settings


def reset():
	global snake
	global food
	global settings

	snake = Snake(starting_coords=[300, 260], resolution=settings["size"] * 10)
	food = Food(resolution=settings["size"] * 10)


def auto_play():
	global auto
	global auto_solve_button
	global screen

	auto = not auto
	auto_solve_button.set_text(screen=screen, text="Auto Solve: {}".format(auto))


def main():
	global running
	global auto
	global auto_solve_button
	global screen
	global snake
	global food
	global settings

	# Loading in the settings
	settings = load_settings()
	screen_resolution = (settings["size"] + np.array([30, 0])) * 10
	speed = settings["game_speed"]

	# Setting the screen options and creating the pygame clock function
	screen = pg.display.set_mode(screen_resolution)
	pg.display.set_caption("Snake")
	clock = pg.time.Clock()

	# Creating the objects on screen
	title = pgparts.TextBox(
		settings["size"][0] * 10 + 50,
		10,
		200,
		80,
		"Snake",
		border_color=pg.Color(100, 100, 100),
		font=pg.font.SysFont("Arial", 50)
	)
	title.set_text(screen=screen, text="Snake", color=pg.Color(255, 255, 255))

	score_board = pgparts.TextBox(
		settings["size"][0] * 10 + 10,
		160,
		200,
		40,
		border_color=pg.Color(100, 100, 100),
		font = pg.font.SysFont("Arial", 25)
	)

	food = Food(resolution=settings["size"] * 10)
	snake = Snake(starting_coords=[300, 260], resolution=settings["size"] * 10)

	reset_button = pgparts.Button(
		x = settings["size"][0] * 10 + 10,
		y = 220,
		width = 100,
		height = 40,
		function = reset,
		text = "",
		border_color=pg.Color(100, 100, 100),
		font=pg.font.SysFont("Arial", 25)
	)
	reset_button.set_text(text = "Reset", color = pg.Color(255, 255, 255), screen=screen)

	auto_solve_button = pgparts.Button(
		x = settings["size"][0] * 10 + 10,
		y = 260,
		width = 200,
		height = 50,
		function = auto_play,
		text = "",
		border_color=pg.Color(100, 100, 100),
		font=pg.font.SysFont("Arial", 25)
	)
	auto_solve_button.set_text(text = "Auto Play: {}".format(auto), color=pg.Color(255, 255, 255), screen=screen)

	game_speed = pgparts.EntryBox(
		x = settings["size"][0] * 10 + 10,
		y = 310,
		width = 200,
		height = 50,
		text = "{}".format(speed),
		max_char = 2,
		border_color=pg.Color(100, 100, 100),
		font=pg.font.SysFont("Arial", 25)
	)


	play_background = pg.Rect(0, 0, settings["size"][0] * 10, settings["size"][1] * 10)
	score_background = pg.Rect(settings["size"][0] * 10, 0, 300, settings["size"][1] * 10 )

	# Running the game loop
	while running:
		try:
			# Handling all the given events
			for event in pg.event.get():
				if event.type == pg.QUIT:
					running = False

				if not auto:
					snake.handle_event(event)

				auto_solve_button.handle_event(event)
				reset_button.handle_event(event)
				game_speed.handle_event(event)

			# Moving and checking the collision
			snake.move()
			snake.check_food_collision(food)

			# Updating the scoreboard
			score_board.set_text(text="Score: {}".format(snake.length - 1), screen=screen, color=pg.Color(255, 255, 255))

			# Coloring in the background
			pg.draw.rect(screen, pg.Color(0, 0, 0), play_background, 0)
			pg.draw.rect(screen, pg.Color(100, 100, 100), score_background, 0)

			# Checking the game speed
			new_speed = int(game_speed.get_text()) if game_speed.get_text() != "" else 0
			if new_speed != speed and new_speed != 0:
				speed = new_speed

			# Drawing the items on screen
			food.draw(screen)
			snake.draw(screen)
			score_board.draw(screen)
			title.draw(screen)
			reset_button.draw(screen)
			auto_solve_button.draw(screen)
			game_speed.draw(screen)

			# updating the gameclock
			pg.display.flip()
			clock.tick(speed)
		except CollisionError as err:
			# Handling the collision error
			snake.direction = ""
			snake.reset()
	return 0

if __name__ == "__main__":
	main()