#!/env python3

class Error(Exception):
	"""
	Base class for exceptions in this module.
	"""
	pass

class CollisionError(Error):
	def __init__(self, message):
		self.message = message